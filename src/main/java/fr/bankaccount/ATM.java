package fr.bankaccount;

import fr.bankaccount.exception.WithdrawLimitException;
import java.util.Scanner;

public class ATM {

    private static Scanner scanner = new Scanner(System.in);

    public static void main(String[] args) {
        var bankAccount = new BankAccount();

        while (true) { //NOSONAR
            try {
                doOperation(bankAccount);
                System.out.println("Operation successfully done");
//            } catch (OverdrawException e) {
//                System.err.println("Overdraw");
            } catch (WithdrawLimitException e) {
                System.err.println("Withdraw limit");
            } finally {
                System.out.println();
            }
        }
    }

    private static void doOperation(BankAccount bankAccount) {
        System.out.print("Operation ([d]eposit/[w]ithdraw): ");
        var command = scanner.next();

        System.out.print("Amount: ");
        var amount = Integer.valueOf(scanner.next());

        if ("d".equals(command)) {
            bankAccount.deposit(amount);
        } else if ("w".equals(command)) {
            bankAccount.withdraw(amount);
        }
    }

}
