package fr.bankaccount;

import fr.bankaccount.exception.WithdrawLimitException;

public class BankAccount {
    private static final int WITHDRAW_LIMIT = 1_000;

    private int balance = 0;

    public void deposit(int amount) {
        balance += amount;
    }

    public void withdraw(int amount) {

//        if (amount >= balance) {
//            throw new OverdrawException();
//        }

        if (amount > WITHDRAW_LIMIT) {
            throw new WithdrawLimitException();
        }

        balance -= amount;
    }
}
