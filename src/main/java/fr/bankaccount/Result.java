package fr.bankaccount;

public sealed interface Result {

    final class Success implements Result {
    }

    final class Overdraw implements Result {
    }

    final class WithdrawLimit implements Result {
    }

}
