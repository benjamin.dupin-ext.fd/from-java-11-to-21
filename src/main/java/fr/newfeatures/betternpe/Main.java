package fr.newfeatures.betternpe;

public class Main {

    public static void main(String[] args) {
        A a = new A();
        var c = a.b.c;
        System.out.println(c);
    }

    static class A {
        B b;

        static class B {
            C c;

            static class C {
            }
        }
    }

}
