package fr.newfeatures.patternmatchinginstanceof;

public class Main {

    public static void main(String[] args) {
        Object anObject = "Hello, world!";

        if (anObject instanceof String aString && aString.length() > 5) {
            System.out.println("It's a String with more than 5 chars");
        }
    }

}
