package fr.newfeatures.recordclass;

public class Main {

    public static void main(String[] args) {
        var john = new RecordClass("John", 25);
        john.sayHello();
    }
}
