package fr.newfeatures.recordclass;

public record RecordClass(String name, int age) {

    public void sayHello() {
        System.out.println("Hi. My name is " + name + " and I'm " + age);
    }

}
