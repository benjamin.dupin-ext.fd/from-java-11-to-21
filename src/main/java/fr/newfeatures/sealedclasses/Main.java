package fr.newfeatures.sealedclasses;

import java.util.List;
import java.util.Random;

public class Main {

    public static void main(String[] args) {
        switch (getARandomShape()) {
            case Circle circle -> {
                System.out.println("It's a circle !");
            }
            case Rectangle rectangle -> {
                System.out.println("It's a rectangle !");
            }
            case Triangle triangle -> {
                System.out.println("It's a triangle !");
            }
        }
    }

    private static Shape getARandomShape() {
        var shapes = List.of(
            new Circle(),
            new Rectangle(),
            new Triangle()
        );

        return shapes.get(
            new Random().nextInt(0, shapes.size())
        );
    }

}
