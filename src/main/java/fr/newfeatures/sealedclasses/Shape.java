package fr.newfeatures.sealedclasses;

public sealed interface Shape permits Circle, Rectangle, Triangle {
}
