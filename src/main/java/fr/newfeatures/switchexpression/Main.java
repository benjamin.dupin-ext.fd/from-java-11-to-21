package fr.newfeatures.switchexpression;


import java.util.Arrays;
import java.util.Random;

public class Main {

    public static void main(String[] args) {
        var randomDay = getARandomDay();
        System.out.println(randomDay + " has " + getNumLetters(randomDay) + " letters");
    }

    private static Day getARandomDay() {
        var days = Arrays.stream(Day.values()).toList();
        return days.get(new Random().nextInt(0, days.size()));
    }

    private static int getNumLetters(Day day) {
        return switch (day) {
            case MONDAY, FRIDAY, SUNDAY -> 6;
            case TUESDAY -> 7;
            default -> {
                String s = day.toString();
                int result = s.length();
                yield result;
            }
        };
    }

    enum Day {
        MONDAY, TUESDAY, WEDNESDAY, THURSDAY, FRIDAY, SATURDAY, SUNDAY;
    }
}

