package fr.newfeatures.textblock;

public class Main {

    private static final String TEXT_BLOCK_STRING =
        """
            <html>

                <body>
                    <span>example text</span>
                </body>
            </html>""";

    private static final String FORMATTED_STRING = "<html>\n"
        + "\n"
        + "    <body>\n"
        + "        <span>example text</span>\n"
        + "    </body>\n"
        + "</html>";

    public static void main(String[] args) {
        System.out.println(
            "String are " +
                (TEXT_BLOCK_STRING.equals(FORMATTED_STRING) ? "" : "not ") +
                "equals"
        );
    }
}